# Globetrotter

Approximate country and language name matching for `pycountry`, making it an even better reference for messy data.

## Motivation

```
>>> import pycountry
>>> pycountry.countries.get(name='Vietnam')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/Users/you/Library/Python/2.7/lib/python/site-packages/pycountry/db.py", line 85, in get
    return self.indices[field][value]
KeyError: 'Vietnam'
>>>
```

'Nuff said.

## Installation

```
pip install globetrotter
```

## Usage

Find a country through the `find_country()` method, giving it an approximate string name. It returns a `Country` object with ISO metadata about the match.

```
>>> import globetrotter
g>>> globetrotter.find_country('Vietnam')
<pycountry.db.Country object at 0x1038eed90>
>>> globetrotter.find_country('Vietnam').name
u'Viet Nam'
```

Find a language through the `find_language()` method in a similar way. It returns a `Language` object.

```
>>> import globetrotter
>>> globetrotter.find_language('Spanish')
<pycountry.db.Language object at 0x103a444d0>
>>> globetrotter.find_language('Spanish').name
u'Spanish; Castilian'
```

