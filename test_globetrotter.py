#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  test_globetrotter.py
#  globetrotter
#

import unittest
import random

import globetrotter

class CountryTestCase(unittest.TestCase):
    def test_exact(self):
        fc = globetrotter.find_country
        for c in ['Australia', 'Spain']:
            self.assertEqual(fc(c).name, c)

    def test_case_mangling(self):
        fc = globetrotter.find_country
        for c in ['Australia', 'Spain']:
            cm = _mangle_case(c)
            self.assertEqual(fc(cm).name, c)

    def test_approx(self):
        fc = globetrotter.find_country
        pairs = [
                ('Vietnam', 'Viet Nam'),
            ]
        for approx, name in pairs:
            self.assertEqual(fc(approx).name, name)

class LanguageTestCase(unittest.TestCase):
    def test_exact(self):
        fl = globetrotter.find_language
        for l in ['English', 'French']:
            self.assertEqual(fl(l).name, l)

    def test_case_mangling(self):
        fl = globetrotter.find_language
        for l in ['English', 'French']:
            ml = _mangle_case(l)
            self.assertEqual(fl(ml).name, l)

    def test_approx(self):
        fl = globetrotter.find_language
        pairs = [
                ('Spanish', 'Spanish; Castilian'),
            ]
        for approx, name in pairs:
            self.assertEqual(fl(approx).name, name)

    def test_modern_ancient(self):
        fl = globetrotter.find_language
        pairs = [
                ('Greek', 'Greek, Modern (1453-)'),
                ('Dutch', 'Dutch; Flemish'),
            ]
        for approx, name in pairs:
            self.assertEqual(fl(approx).name, name)

    def test_aliases(self):
        fl = globetrotter.find_language
        pairs = [
                ('Mandarin', 'Chinese'),
                ('Mandarin Chinese', 'Chinese'),
            ]
        for approx, name in pairs:
            self.assertEqual(fl(approx).name, name)


def _mangle_case(s):
    if len(s) == 0:
        return s

    i = _mangle_case._r.choice(xrange(len(s)))
    c = s[i].lower() if s[i].isupper() else s[i].upper()
    return s[:i] + c + s[(i+1):]

_mangle_case._r = random.Random(1001) # seed it manually

def suite():
    return unittest.TestSuite(map(unittest.makeSuite, [
            CountryTestCase,
            LanguageTestCase,
        ]))

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=1).run(suite())
